/**
 * Created by Marcio Costa on 01/04/14.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {                                       // Minificador de js
            'js/main.js': ['js/jquery-1.11.0.min.js',  // Paths que formam o main.js
                'js/bootstrap.min.js',
                'js/html5shiv.js',
                'js/respond.js',
                'js/miscellaneous.js']
        },

        cssmin: {                                       // Minificador de css

            'css/util.css': ['css/bootstrap.min.css',  // Paths que formam o util.css
                'css/bootstrap-theme.min.css',
                'css/fontface.css',
                'css/font-awesome.min.css'],
            options:{
                keepSpecialComments: 1
            }
        },

        less: {                                         // Compilador de less
            'css/estrutura.css': ['less/main.less'],
            options:{
                cleancss: true                          // Remove Comentários
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'img/',                   // Dir das imagens
                    src: ['*.{png,jpg}'],          // formatos que irão ser otimizados
                    dest: 'img/build'              // Dir de destino
                }]
            }
        }

    });

    // Carrega as dependencias dos módulos de grunt

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    //tarefa default//

    grunt.registerTask('default', ['uglify','cssmin','less','imagemin']);


    //tarefa less+imagens//

    grunt.registerTask('less-img', ['less','imagemin']);



};